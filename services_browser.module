<?php

/**
 * @file
 * service_browser module implementation
 */


/**
 * Implements hook_permission().
 */
function services_browser_permission() {

  return array(
    'use service browser' => array(
      'title' => 'Use service browser',
      'description' => 'Allows the use of the Service Browser which permits the invocation of any enabled Services resource operation',
      'restrict access' => TRUE,
    ),
  );

}

/**
 * Implements hook_help().
 */
function services_browser_help($path, $arg) {
  switch ($path) {
    case 'admin/structure/servicesbrowser':
      $help = t('<p>Services are collections of resources exposing CRUD access and additional methods available to
        remote applications. They are defined in modules, and may be accessed in a number of ways through
        server modules. Visit the <a href="@documentation">Services Handbook</a> for help and information.</p>

        <p>All enabled services and methods are shown. Click on any method to view information or test.</p>

        <p>Visit the service <a href="@admin">Services administrative section</a> to enable server endpoints and
        resource access</p>',
               array(
                 '@documentation' => 'http://drupal.org/node/736522',
                 '@admin' => url('admin/structure/services'),
               )
      );
      return $help;

    case 'admin/structure/servicesbrowser/operation/%/%':
      return t('<p>You can call this operation by providing its parameters, if any, and submit.</p>');
  }
}

/**
 * Implements hook_menu().
 */
function services_browser_menu() {

  $items = array();

  $items['admin/structure/servicesbrowser'] = array(
    'title' => 'Services Browser',
    'description' => 'Services endpoints resource browser and debugging tool',
    'page callback' => 'services_browser_overview_page',
    'access arguments' => array('use service browser'),
  );

  $items['admin/structure/servicesbrowser/operation/%/%/%'] = array(
    'title' => 'Services Browser',
    'description' => 'Services endpoints resource browser and debugging tool',
    'page callback' => 'services_browser_operation_page',
    'page arguments' => array(4, 5, 6),
    'access arguments' => array('use service browser'),
  );

  return $items;

}

/**
 * Resource operations overview page callback.
 *
 * Return a listing page of all resource operations with a link to the
 * operation invocation page.
 *
 * @return string
 *   Overview page content as HTML markup
 */
function services_browser_overview_page() {

  $resources = services_get_resources();

  foreach ($resources as $resource_name => $operation_groups) {

    $resource_item = array(
      'data' => $resource_name,
    );

    foreach ($operation_groups as $group_name => $operations) {
      $resource_item['children'][$group_name] = array(
        'data' => $group_name,
      );
      foreach ($operations as $operation_name => $operation) {
        if (isset($operation['callback'])) {
          $resource_item['children'][$group_name]['children'][$operation_name] = array(
            'data' => l(
              $operation_name,
              'admin/structure/servicesbrowser/operation/' . $resource_name . '/' . $group_name . '/' . $operation_name
            ),
          );
        }
      }
    }

    $resource_items[] = $resource_item;

  }

  return theme(
    'item_list',
    array(
      'title' => 'Resources',
      'items' => $resource_items,
      'type' => 'ul',
    )
  );

}

/**
 * Resource operation invocation page.
 *
 * Return a page containing the invocation form and the last invocation result
 * if any.
 *
 * @return array
 *   Operation page content as elements array.
 */
function services_browser_operation_page($resource_name, $group_name, $operation_name) {

  $resource_name = check_plain($resource_name);
  $group_name = check_plain($group_name);
  $operation_name = check_plain($operation_name);

  $resources = services_get_resources();
  if (isset($resources[$resource_name][$group_name][$operation_name]['callback'])) {
    $operation = $resources[$resource_name][$group_name][$operation_name];
  }
  else {
    drupal_not_found();
    return;
  }

  drupal_set_title(t('Resource @resource_name, operation @operation_name',
                     array('@resource_name' => $resource_name, '@operation_name' => $operation_name)));

  $page['operation_form'] = drupal_get_form('services_browser_operation_form', $operation);

  // Get the result of a previous submission if any.
  if (isset($_SESSION['services_browser_result_output'])) {
    $page['operation_result'] = array('#markup' => $_SESSION['services_browser_result_output']);
    unset($_SESSION['services_browser_result_output']);
  }

  return $page;

}

/**
 * Form constructor for the operation form.
 *
 * @param array $form
 *   The form definition array as required by hook_form()
 * @param array &$form_state
 *   The form state array as required by hook_form()
 * @param array $operation
 *   The operation for which this operation form is generated
 *
 * @return array
 *   The form definition array
 *
 * @see services_browser_operation_form_submit()
 * @ingroup forms
 */
function services_browser_operation_form($form, &$form_state, $operation) {

  $form['#theme'] = 'services_browser_operation_form';

  $form['args']['#tree'] = TRUE;
  $form['format']['#tree'] = TRUE;
  $form['required']['#tree'] = TRUE;

  if (isset($operation['args'])) {

    foreach ($operation['args'] as $argument) {

      $form['args'][$argument['name']] = array(
        '#type' => 'textfield',
        '#title' => check_plain($argument['name']),
        '#description' => check_plain(isset($argument['description']) ? $argument['description'] : ''),
        '#required' => isset($argument['optional']) ? !$argument['optional'] : TRUE,
        '#default_value' => isset($form_state['values']['args'][$argument['name']]) ? $form_state['values']['args'][$argument['name']] : NULL,
      );

      $format_opt = array();
      switch ($argument['type']) {
        case 'array':
          $format_opt['cdel'] = t('Comma delimited');
        case 'struct':
          $format_opt['json'] = t('JSON');
          $format_opt['sphp'] = t('Serialized PHP');
          break;

      }
      if (!empty($format_opt)) {
        $form['format'][$argument['name']] = array(
          '#type' => 'select',
          '#title' => check_plain($argument['name']) . ' format',
          '#options' => $format_opt,
        );
      }
      else {
        $form['format'][$argument['name']] = array(
          '#type' => 'value',
          '#value' => '',
        );
      }

      $form['required'][$argument['name']] = array(
        '#type' => 'markup',
        '#markup' => (!isset($argument['optional']) || !$argument['optional']) ? 'required' : 'optional',
      );

    }

  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Call method',
  );

  return $form;

}


/**
 * Implements hook_FORM_submit().
 */
function services_browser_operation_form_submit(&$form, &$form_state) {

  $operation = $form_state['build_info']['args'][0];

  $callback = $operation['callback'];
  $callback_arguments = array();

  if (isset($operation['args'])) {
    foreach ($operation['args'] as $argument) {

      if (isset($form_state['values']['args'][$argument['name']])) {
        $argument_input_value = $form_state['values']['args'][$argument['name']];
        if ($argument_input_value === '0' || !empty($argument_input_value)) {
          $argument_value = $argument_input_value;
        }
        unset($argument_input_value);
      }

      if (!isset($argument_value) && (!isset($argument['optional']) || !$argument['optional'])) {
        $argument_value = '';
      }

      if (isset($argument_value)) {

        if (isset($form_state['values']['format'][$argument['name']])) {
          $format = $form_state['values']['format'][$argument['name']];
        }

        $formats = array('', 'cdel', 'json', 'sphp');
        if (!isset($format) || !in_array($format, $formats)) {
          $format = '';
        }

        $callback_arguments[] = services_browser_unserialize_argument($format, $argument_value, $argument['type']);

        unset($format);
        unset($argument_value);

      }
      else {
        break;
      }
    }
  }

  module_load_include('inc', 'services', 'services.runtime');
  if (!empty($operation['file'])) {
    // Convert old-style file references to something that
    // fits module_load_include() better.
    if (!empty($operation['file']['file']) && empty($controller['file']['type'])) {
      $operation['file']['type'] = $controller['file']['file'];
    }
    module_load_include($operation['file']['type'], $operation['file']['module'], $operation['file']['name']);
  }
  $result = call_user_func_array($callback, $callback_arguments);

  if (function_exists('kpr')) {
    $result_output = kpr($result, TRUE);
  }
  else {
    $result_output = '<pre>' . htmlspecialchars(print_r($result, TRUE)) . '</pre>';
  }

  // Temporarily store result in session to display it on the result page.
  $_SESSION['services_browser_result_output'] = $result_output;
}

/**
 * Unserialize a given argument from the form user input for a given format.
 *
 * @param string $format
 *   the serialization format
 * @param string $value
 *   the user-input value
 * @param string $type
 *   the type of the operation argument
 *
 * @return array|mixed|null
 *   the data structure
 */
function services_browser_unserialize_argument($format, $value, $type) {
  switch ($format) {
    case 'cdel';
      if (empty($value)) {
        return NULL;
      }
      else {
        return explode(',', $value);
      }
      break;

    case 'json':
      if (empty($value)) {
        return NULL;
      }
      else {
        return json_decode($value, $type === 'array');
      }
      break;

    case 'sphp':
      if (empty($value)) {
        return NULL;
      }
      else {
        return unserialize($value);
      }
      break;

    default:
      return $value;
  }
}

/**
 * Implements hook_theme().
 */
function services_browser_theme() {
  return array(
    'services_browser_operation_form' => array(
      'render element' => 'form',
    ),
  );
}

/**
 * Returns HTML for an operation form.
 *
 * The operation form is rendered as a table.
 *
 * @param array $variables
 *   An associative array containing:
 *   - form: The operation form definition
 *
 * @ingroup themeable
 */
function theme_services_browser_operation_form($variables) {

  $form = $variables['form'];

  $header = array('Name', 'Required', 'Value', 'Format');

  $table = '';

  if (isset($form['args'])) {

    $rows = array();

    foreach (element_children($form['args']) as $key => $type) {

      $name = $form['args'][$type]['#title'];
      unset($form['args'][$type]['#title']);

      $input = drupal_render($form['args'][$type]);
      unset($form['format'][$type]['#title']);
      $format = drupal_render($form['format'][$type]);

      $required = drupal_render($form['required'][$type]);

      $rows[] = array($name, $required, $input, $format);

    }

    $table = theme('table', array('header' => $header, 'rows' => $rows));
  }

  $form_footer = drupal_render_children($form);

  return $table . $form_footer;

}
